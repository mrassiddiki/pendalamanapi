package mahasiswa

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"time"

	"apigomysql/config"
	"apigomysql/model"
)

const (
	table          = "mahasiswa"
	layoutDateTime = "2006-01-02 15:04:05"
)

func GetAll(ctx context.Context) ([]model.Mahasiswa, error) {
	var mahasiswas []model.Mahasiswa

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant Connect to MySQL ", err)
	}

	queryText := fmt.Sprintf("SELECT * FROM %v ORDER BY id DESC", table)

	rowQuery, err := db.QueryContext(ctx, queryText)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa model.Mahasiswa
		var createdAt, updatedAt string

		if err = rowQuery.Scan(&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		mahasiswas = append(mahasiswas, mahasiswa)
	}

	return mahasiswas, nil

}

func Insert(ctx context.Context, mhs model.Mahasiswa) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant Connect to Database", err)
	}

	queryText := fmt.Sprintf("INSERT INTO %v (nim, name, semester, created_at, updated_at) values(%v, '%v', '%v', '%v', '%v')", table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester,
		time.Now().Format(layoutDateTime),
		time.Now().Format(layoutDateTime))
	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}
	return nil

}

func Update(ctx context.Context, mhs model.Mahasiswa) error {
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant Connect to Database", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set nim = %d, name = '%s', semester = '%s', updated_at = '%v' where id = '%d'",
		table,
		mhs.NIM,
		mhs.Name,
		mhs.Semester,
		time.Now().Format(layoutDateTime),
		mhs.ID,
	)

	fmt.Println(queryText)
	s, err := db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check < 1 {
		return errors.New("ID Mahasiswa Tidak Ditemukan")
	}
	return nil

}

func Delete(ctx context.Context, mhs model.Mahasiswa) ([]model.Mahasiswa, error) {
	var mahasiswas []model.Mahasiswa
	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Cant Connect to Database", err)
	}

	querySelect := fmt.Sprintf("SELECT * FROM %v WHERE ID = %d ", table, mhs.ID)

	rowQuery, err := db.QueryContext(ctx, querySelect)

	if err != nil {
		log.Fatal(err)
	}

	for rowQuery.Next() {
		var mahasiswa model.Mahasiswa
		var createdAt, updatedAt string

		if err = rowQuery.Scan(&mahasiswa.ID,
			&mahasiswa.NIM,
			&mahasiswa.Name,
			&mahasiswa.Semester,
			&createdAt,
			&updatedAt); err != nil {
			return nil, err
		}

		mahasiswa.CreatedAt, err = time.Parse(layoutDateTime, createdAt)

		if err != nil {
			log.Fatal(err)
		}

		mahasiswa.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)

		if err != nil {
			log.Fatal(err)
		}

		mahasiswas = append(mahasiswas, mahasiswa)
	}

	queryText := fmt.Sprintf("DELETE FROM %v WHERE ID = %d", table, mhs.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return nil, errors.New("ID Mahasiswa Tidak Ditemukan")
	}

	return mahasiswas, nil

}
